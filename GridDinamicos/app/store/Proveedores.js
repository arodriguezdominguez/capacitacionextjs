
Ext.define('TFE.store.Proveedores', {                     
                    extend: 'Ext.data.Store',
                    model: 'TFE.model.Proveedor',

                    buffered : false,
//                    autoLoad: true,
                    pageSize: 4,

    proxy: {

        type: 'ajax',
        url: 'data/pagingProveedores.php',
//        url: 'http://localhost/pagingProveedores.php',
//        enablePaging: true,

//        api: {        
//            read: 'data/Proveedores.json',    
//           update: 'data/updateProveedores.json'
//        },

        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success'
        },

        baseParams: {z:'0',start:'0',limit:'4'} //<--- parámetros definidos
    },


                    listeners: {

                        load: function() {

                            console.log('pase por listeners--> '+ this.currentPage);

                        }
                    } 


}); 

