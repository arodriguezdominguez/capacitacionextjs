Ext.define('TFE.store.Inventarios', {
    extend: 'Ext.data.Store',
    model: 'TFE.model.Inventario',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        api: {
            read: 'data/Inventarios.json',
           update: 'data/updateInventarios.json'
        },

        reader: {
            type: 'json',
            root: 'Inventarios',
            successProperty: 'success'
        }
    }
});