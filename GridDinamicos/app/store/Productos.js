Ext.define('TFE.store.Productos', {
    extend: 'Ext.data.Store',
    model: 'TFE.model.Producto',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        api: {
            read: 'data/Productos.json',
           update: 'data/updateProductos.json'
        },

        reader: {
            type: 'json',
            root: 'Productos',
            successProperty: 'success'
        }
    }
});