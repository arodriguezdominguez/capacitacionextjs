Ext.define('TFE.store.Tiendas', {
    extend: 'Ext.data.Store',
    model: 'TFE.model.Tienda',
    autoLoad: true,

    proxy: {
        type: 'ajax',
        api: {
            read: 'data/Tiendas.json',
           update: 'data/updateTiendas.json'
        },

        reader: {
            type: 'json',
            root: 'Tiendas',
            successProperty: 'success'
        }
    }
});