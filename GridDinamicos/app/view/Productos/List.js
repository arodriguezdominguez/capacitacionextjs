Ext.define('TFE.view.Productos.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.Productoslista',
    title: 'Lista de Productos',
    store: 'Productos',


    initComponent: function() {


        console.log('init function en Productos LIST');
        this.columns = [
            {header: 'Codigo Producto',  dataIndex: 'CodProducto',  flex: 1},
            {header: 'Codigo Proveedor',  dataIndex: 'CodProveedor',  flex: 1},
            {header: 'Nombre Producto',  dataIndex: 'Nombre',  flex: 1},
            {header: 'Descripcion', dataIndex: 'Descripcion', flex: 1}           

        ];

        this.callParent(arguments);
        
        this.hide();


    },
     Mostrar: function() {
        this.show();


        console.log('estoy en Mostrar Producto');

                            },
     Ocultar: function() {
        this.hide();

        console.log('estoy en Ocultar Producto');

                            }   
 });        
