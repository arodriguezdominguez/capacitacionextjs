Ext.define('TFE.view.Productos.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.productoedit',

    title: 'Editar Producto',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                width: 310,
                height: 150,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'CodProducto',
                        fieldLabel: 'Codigo Producto'
                    },
                    {
                        xtype: 'combobox',
                        fieldLabel: 'Codigo Proveedor',
                        store: 'Proveedores',
                        queryMode: 'local',
                        displayField: 'Compania',
                        valueField: 'CodProveedor',
                        name : 'CodProveedor',                       
                    },
                    {
                        xtype: 'textfield',
                        name : 'Nombre',
                        fieldLabel: 'Nombre Producto'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Descripcion',
                        fieldLabel: 'Descripcion'
                    }
 

                ]
            }
        ];

        this.buttons = [

            {
                text: 'Crear',
                action: 'create'
            },


            {
                text: 'Actualizar',
                action: 'update'
            },
            {
                text: 'Eliminar',
                action: 'delete'
            },

            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});