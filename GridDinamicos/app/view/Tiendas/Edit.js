Ext.define('TFE.view.Tiendas.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.tiendaedit',

    title: 'Editar Tienda',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                width: 310,
                height: 150,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'CodTienda',
                        fieldLabel: 'Codigo Tienda'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Nombre',
                        fieldLabel: 'Nombre Tienda'
                    },
                    {
                        xtype: 'textfield',
                        name : 'PContacto',
                        fieldLabel: 'Persona Contacto'
                    },
                    {
                        xtype: 'textfield',
                        name : 'email',
                        fieldLabel: 'Email'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Telefono',
                        fieldLabel: 'Telefono'
                    }
                ]
            }
        ];

        this.buttons = [

            {
                text: 'Crear',
                action: 'create'
            },


            {
                text: 'Actualizar',
                action: 'update'
            },
            {
                text: 'Eliminar',
                action: 'delete'
            },

            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});