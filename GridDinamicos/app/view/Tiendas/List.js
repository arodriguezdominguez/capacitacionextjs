Ext.define('TFE.view.Tiendas.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.Tiendaslista',

    title: 'Lista de Tiendas',

    store: 'Tiendas',



    initComponent: function() {

        console.log('init function en Tiendas LIST');
        this.columns = [
            {header: 'Codigo Tienda',  dataIndex: 'CodTienda',  flex: 1},
            {header: 'Nombre Tienda',  dataIndex: 'Nombre',  flex: 1},
            {header: 'Persona Contacto',  dataIndex: 'PContacto',  flex: 1},
            {header: 'Email', dataIndex: 'email', flex: 1},           
            {header: 'Telefono Contacto', dataIndex: 'Telefono', flex: 1}
        ];
        this.callParent(arguments);

            this.hide();
        

    },
      Mostrar: function () {

        console.log('estoy en Mostrar Tiendas');
        this.show();
                            },
                            
      Ocultar: function () {

        console.log('estoy en Ocultar Tiendas');
        this.hide();
                            }
});