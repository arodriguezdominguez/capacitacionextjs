Ext.define('TFE.view.Inventarios.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.Inventarioslista',

    title: 'Lista de Inventarios en Tiendas',

    store: 'Inventarios',



    initComponent: function() {

        console.log('init function en Inventarios LIST');
        this.columns = [
            {header: 'Codigo Inventario',  dataIndex: 'CodInventario',  flex: 1},
            {header: 'Codigo Tienda',  dataIndex: 'CodTienda',  flex: 1},
            {header: 'Codigo Producto',  dataIndex: 'CodProducto',  flex: 1},
            {header: 'Cantidad', dataIndex: 'Cantidad', flex: 1},           
            {header: 'Fecha de Compra', dataIndex: 'FechaCompra', flex: 1}
        ];
        this.callParent(arguments);

            this.hide();
        

    },
      Mostrar: function () {

        console.log('estoy en Mostrar Inventarios');
        this.show();
                            },
                            
      Ocultar: function () {

        console.log('estoy en Ocultar Inventarios');
        this.hide();
                            }
});