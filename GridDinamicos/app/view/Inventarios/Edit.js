Ext.define('TFE.view.Inventarios.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.inventarioedit',

    title: 'Editar Inventario',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                width: 310,
                height: 150,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'CodInventario',
                        fieldLabel: 'Codigo Inventario'
                    },
                    {
                        xtype: 'combobox',
                        fieldLabel: 'Codigo Tienda',
                        store: 'Tiendas',
                        queryMode: 'local',
                        displayField: 'Nombre',
                        valueField: 'CodTienda',
                        name : 'CodTienda',                       
                    },
                    {
                        xtype: 'combobox',
                        fieldLabel: 'Codigo Producto',
                        store: 'Productos',
                        queryMode: 'local',
                        displayField: 'Nombre',
                        valueField: 'CodProducto',
                        name : 'CodProducto',                       
                    },
                    {
                        xtype: 'textfield',
                        name : 'Cantidad',
                        fieldLabel: 'Cantidad'
                    },
                    {
                        xtype: 'datefield',
                        format: 'd/m/Y',                        
                        inputAttrTpl: " data-qtip='Coloque la fecha que aparece en la Factura de Compra!' ",                       
                        name : 'FechaCompra',
                        fieldLabel: 'Fecha de Compra'
                    }
                ]
            }
        ];

        this.buttons = [

            {
                text: 'Crear',
                action: 'create'
            },


            {
                text: 'Actualizar',
                action: 'update'
            },
            {
                text: 'Eliminar',
                action: 'delete'
            },

            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});