Ext.define('TFE.view.Proveedores.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.proveedoredit',

    title: 'Editar Proveedor',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                width: 310,
                height: 150,
                items: [
                    {
                        xtype: 'textfield',
                        name : 'CodProveedor',
                        fieldLabel: 'Codigo Proveedor'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Compania',
                        fieldLabel: 'Compania'
                    },
                    {
                        xtype: 'textfield',
                        name : 'PContacto',
                        fieldLabel: 'Persona Contacto'
                    },
                    {
                        xtype: 'textfield',
                        name : 'email',
                        fieldLabel: 'Email'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Telefono',
                        fieldLabel: 'Telefono'
                    }
                ]
            }
        ];

        this.buttons = [

            {
                text: 'Crear',
                action: 'create'
            },


            {
                text: 'Actualizar',
                action: 'update'
            },
            {
                text: 'Eliminar',
                action: 'delete'
            },

            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});