Ext.define('TFE.view.Proveedores.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.Proveedoreslista',

    title: 'Lista de Proveedores',
    height: 170,

      store: 'Proveedores',

    dockedItems: [{
        itemId: 'paginador',
        xtype: 'pagingtoolbar',
        pageSize: 4,
        start:0,
        limit:4,
        displayMsg: 'Registros: {0} al {1} de {2} ',
        emptyMsg: 'No hay datos',
        store: 'Proveedores',    // same store GridPanel is using

        listeners: {
                beforechange: function(bar,params){
                    bar.z = params;
//                    bar.start = params;
                    console.log('pase por beforechange--> '+ params+' bar.z--> '+bar.z);

                this.store.load({'params': {'z':bar.z.toString(),'start':'0','limit':'4'}});

                        }
                    },

        dock: 'top', //bottom',
        displayInfo: true

    }],    

    initComponent: function() {
        console.log('init function en Proveedores LIST');
        this.columns = [
            {header: 'Codigo Proveedor',  dataIndex: 'CodProveedor',  flex: 1},
            {header: 'Compania',  dataIndex: 'Compania',  flex: 1},
            {header: 'Persona Contacto',  dataIndex: 'PContacto',  flex: 1},
            {header: 'Email', dataIndex: 'email', flex: 1},           
            {header: 'Telefono Contacto', dataIndex: 'Telefono', flex: 1}
        ];
        this.callParent(arguments);


        

        this.hide();
        

    },
    Mostrar: function () {

        console.log('estoy en Mostrar Proveedores');
        this.show();

                            },
    Ocultar: function () {

        console.log('estoy en Ocultar Proveedores');
        this.hide();
                            },

    Cargar: function () {
                var paginadorctrl=null;
                console.log('estoy en Cargar Proveedores Ya');

//                this.store.load({params:{start:0,limit:4}});

                this.store.load({params:{z:'0',start:'0',limit:'4'}});
//                this.store.load({params:{z:10}});
//                this.store.load();

                } 

                                          
}); 
