Ext.define('TFE.controller.Proveedor', {
    extend: 'Ext.app.Controller',

    views: [
        'Proveedores.List',
        'Proveedores.Edit'        
    ],

    stores: 'Proveedores',
    models: 'Proveedor',

    init: function() {
        this.control({            

            'proveedoredit button[action=delete]': {
                click: this.deleteProveedor
            },

            'proveedoredit button[action=create]': {
                click: this.createProveedor
            },


            'proveedoredit button[action=update]': {
                click: this.updateProveedor
            },

            'Proveedoreslista': {
                itemdblclick: this.editProveedor
            }

        });
    },

    updateProveedor: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

    record.set(values);
    win.close();
    // synchronize the store after editing the record
    this.getProveedoresStore().sync();

    },

    editProveedor: function(grid, record) {
        var view = Ext.widget('proveedoredit');

        view.down('form').loadRecord(record);
    },

    deleteProveedor: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord();    
    console.log('Entre en delete proveedor ->'+ record.get('Compania'));
    win.close();
    this.getProveedoresStore().remove(record);

    // synchronize the store after editing the record
    this.getProveedoresStore().sync();

    },

    createProveedor: function(button) {
        var win    = button.up('window');
        win.close();
        var proveedorStore = this.getProveedoresStore();
        var proveedorModel = Ext.create('TFE.model.Proveedor');

        proveedorModel.set("CodProveedor", "00");
        proveedorModel.set("Compania", "Nueva Compania");
        proveedorModel.set("PContacto", "Nueva persona contacto");
        proveedorModel.set("email", "Nuevo correo electronico");
        proveedorModel.set("Telefono", "Nuevo Telefono");

        proveedorStore.add(proveedorModel);

//        proveedorGrid.editingPlugin.startEdit(proveedorModel, 3);
    // synchronize the store after editing the record
    this.getProveedoresStore().sync();

    },


});