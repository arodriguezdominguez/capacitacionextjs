Ext.define('TFE.controller.Inventario', {
    extend: 'Ext.app.Controller',

    views: [
        'Inventarios.List',
        'Inventarios.Edit'       
    ],

    stores: 'Inventarios',
    models: 'Inventario',

    init: function() {
        this.control({

            'inventarioedit button[action=delete]': {
                click: this.deleteInventario
            },

            'inventarioedit button[action=create]': {
                click: this.createInventario
            },


            'inventarioedit button[action=update]': {
                click: this.updateInventario
            },

            'Inventarioslista': {
                itemdblclick: this.editInventario
            }
        });
    },

    updateInventario: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

    record.set(values);
    win.close();
    // synchronize the store after editing the record
    this.getInventariosStore().sync();

    },

    editInventario: function(grid, record) {
        var view = Ext.widget('inventarioedit');

        view.down('form').loadRecord(record);
    },

    deleteInventario: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord();    
    console.log('Entre en delete Inventario ->');
    win.close();
    this.getInventariosStore().remove(record);

    // synchronize the store after editing the record
    this.getInventariosStore().sync();

    },

    createInventario: function(button) {
        var win    = button.up('window');
        win.close();
        var inventarioStore = this.getInventariosStore();
        var inventarioModel = Ext.create('TFE.model.Inventario');

        inventarioModel.set("CodInventario", "00");
        inventarioModel.set("CodTienda", "00");
        inventarioModel.set("CodProducto", "00");
        inventarioModel.set("Cantidad", "Cantidad");
        inventarioModel.set("FechaCompra", "Fecha de Compra");

        inventarioStore.add(inventarioModel);

//        proveedorGrid.editingPlugin.startEdit(proveedorModel, 3);


    // synchronize the store after editing the record
    this.getInventariosStore().sync();

    }

});