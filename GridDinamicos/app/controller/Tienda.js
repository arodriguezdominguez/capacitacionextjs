Ext.define('TFE.controller.Tienda', {
    extend: 'Ext.app.Controller',

    views: [
        'Tiendas.List',
        'Tiendas.Edit'        
    ],

    stores: 'Tiendas',
    models: 'Tienda',

    init: function() {
        this.control({

            'tiendaedit button[action=delete]': {
                click: this.deleteTienda
            },

            'tiendaedit button[action=create]': {
                click: this.createTienda
            },


            'tiendaedit button[action=update]': {
                click: this.updateTienda
            },

            'Tiendaslista': {
                itemdblclick: this.editTienda
            }
        });
    },

    updateTienda: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

    record.set(values);
    win.close();
    // synchronize the store after editing the record
    this.getTiendasStore().sync();

    },

    editTienda: function(grid, record) {
        var view = Ext.widget('tiendaedit');

        view.down('form').loadRecord(record);
    },

    deleteTienda: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord();    
    console.log('Entre en delete Tienda ->'+ record.get('Nombre'));
    win.close();
    this.getTiendasStore().remove(record);

    // synchronize the store after editing the record
    this.getTiendasStore().sync();

    },

    createTienda: function(button) {
        var win    = button.up('window');
        win.close();
        var tiendaStore = this.getTiendasStore();
        var tiendaModel = Ext.create('TFE.model.Tienda');

        tiendaModel.set("CodTienda", "00");
        tiendaModel.set("Nombre", "Nombre Nueva Tienda");
        tiendaModel.set("PContacto", "Nueva persona contacto");
        tiendaModel.set("email", "Nuevo correo electronico");
        tiendaModel.set("Telefono", "Nuevo Telefono");

        tiendaStore.add(tiendaModel);

//        proveedorGrid.editingPlugin.startEdit(proveedorModel, 3);


    // synchronize the store after editing the record
    this.getTiendasStore().sync();

    }

});