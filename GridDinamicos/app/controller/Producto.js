Ext.define('TFE.controller.Producto', {
    extend: 'Ext.app.Controller',

    views: [
        'Productos.List',
        'Productos.Edit'       
    ],

    stores: 'Productos',
    models: 'Producto',

    init: function() {
        this.control({

            'productoedit button[action=delete]': {
                click: this.deleteProducto
            },

            'productoedit button[action=create]': {
                click: this.createProducto
            },


            'productoedit button[action=update]': {
                click: this.updateProducto
            },

            'Productoslista': {
                itemdblclick: this.editProducto
            }
        });
    },

    updateProducto: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

    record.set(values);
    win.close();
    // synchronize the store after editing the record
    this.getProductosStore().sync();

    },

    editProducto: function(grid, record) {
        var view = Ext.widget('productoedit');

        view.down('form').loadRecord(record);
    },

    deleteProducto: function(button) {
    var win    = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord();    
    console.log('Entre en delete Producto ->');
    win.close();
    this.getProductosStore().remove(record);

    // synchronize the store after editing the record
    this.getProductosStore().sync();

    },

    createProducto: function(button) {
        var win    = button.up('window');
        win.close();
        var productoStore = this.getProductosStore();
        var productoModel = Ext.create('TFE.model.Producto');

        productoModel.set("CodProducto", "00");
        productoModel.set("CodProveedor", "00");
        productoModel.set("Nombre", "Nuevo Producto");
        productoModel.set("Descripcion", "poner alguna descripcion");


        productoStore.add(productoModel);

//        proveedorGrid.editingPlugin.startEdit(proveedorModel, 3);


    // synchronize the store after editing the record
    this.getProductosStore().sync();

    }

});