Ext.define('TFE.model.Inventario', {
    extend: 'Ext.data.Model',
    fields: ['CodInventario','CodTienda','CodProveedor','CodProducto','Cantidad', {name:'FechaCompra',type: 'sdate',format: 'd/m/Y'}]
});