Ext.define('TFE.model.Producto', {
    extend: 'Ext.data.Model',
    fields: ['CodProducto','CodProveedor','Nombre','Descripcion']
});