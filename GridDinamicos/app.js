
Ext.application({
    

    requires: ['Ext.container.Viewport'],
    name: 'TFE',
    appFolder: 'app',
    
    controllers: [
        'Proveedor',
        'Producto',
        'Tienda',
        'Inventario'
    ],

    launch: function() {
var miclase = null;

var childPanel1 = Ext.create('Ext.panel.Panel', {
            width: '100%',
            height: 25,
            title: '<center>TEST FRONT END en Ext JS</center>',

});




ventana=Ext.create('Ext.container.Viewport', {


    items: [ childPanel1, {
    xtype: 'combobox',
    emptyText:'Seleccione la Tabla a Modificar',
//    fieldLabel: '<center>Seleccione la Tabla a Modificar</center>',
    hiddenName: 'selector',
    width: 200,
    store: new Ext.data.SimpleStore({
        data: [
            [1, 'Proveedores'],
            [2, 'Productos'],
            [3, 'Tiendas'],
            [4, 'Inventarios'],
        ],
        id: 0,
        fields: ['value', 'text']
    }),
    valueField: 'value',
    displayField: 'text',
    triggerAction: 'all',
    listeners:{
     //    scope: yourScope,
         'select': function(){ 
            switch(this.getValue()) {
                case 1:
                console.log('funciona Proveedor->'+ this.getValue());
                miclase = ventana.getComponent('inventario');
                miclase.Ocultar();
                miclase = ventana.getComponent('tienda');
                miclase.Ocultar();
                miclase = ventana.getComponent('producto');
                miclase.Ocultar();
                miclase = ventana.getComponent('proveedor');
                miclase.Cargar();
                miclase.Mostrar();                
                break;
                case 2:
                console.log('funciona Producto->'+ this.getValue());
                miclase = ventana.getComponent('inventario');
                miclase.Ocultar();                
                miclase = ventana.getComponent('tienda');
                miclase.Ocultar();
                miclase = ventana.getComponent('proveedor');
                miclase.Ocultar();
                miclase = ventana.getComponent('producto');            
                miclase.Mostrar();                
                break;
                case 3:
                console.log('funciona Tienda->'+ this.getValue());
                miclase = ventana.getComponent('inventario');
                miclase.Ocultar();                
                miclase = ventana.getComponent('proveedor');
                miclase.Ocultar();
                miclase = ventana.getComponent('producto');
                miclase.Ocultar();
                miclase = ventana.getComponent('tienda');                            
                miclase.Mostrar();                
                break;
                case 4:
                console.log('funciona Inventario->'+ this.getValue());
                miclase = ventana.getComponent('proveedor');
                miclase.Ocultar();
                miclase = ventana.getComponent('producto');
                miclase.Ocultar();
                miclase = ventana.getComponent('tienda');
                miclase.Ocultar();
                miclase = ventana.getComponent('inventario');                            
                miclase.Mostrar();                
                break;
            }

        }
     },
    editable: false
}, {itemId: 'proveedor', xtype: 'Proveedoreslista'},
{itemId: 'producto', xtype: 'Productoslista'},
{itemId: 'tienda', xtype: 'Tiendaslista'},
{itemId: 'inventario', xtype: 'Inventarioslista'}]

});
}
});

